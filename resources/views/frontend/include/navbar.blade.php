<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header customLogo">
{{--            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1">--}}
{{--                <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span--}}
{{--                    class="icon-bar"></span><span class="icon-bar"></span></button>--}}
            <a class="navbar-brand" href="{{ route('index') }}"><img src="{{ asset('frontend/images/logo.png') }}"
                                                                     style="width: 50%"
                                                                     alt=""></a></div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="defaultNavbar1">
            <ul class="nav navbar-nav">
                @guest
                    <li class="hidden-lg hidden-md hidden-sm"><a href="{{ route('login') }}"
                                                                 class=" {{ (request()->is('login')) ? 'active' : '' }}">Login</a>
                    </li>
                @endguest


            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
