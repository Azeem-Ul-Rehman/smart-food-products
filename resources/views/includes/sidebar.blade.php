<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i
        class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
         m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">


            <li class="m-menu__item {{ (request()->is('admin/dashboard')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.dashboard.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-dashboard"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Dashboard</span>
											</span></span></a></li>
            <li class="m-menu__item  {{ (request()->is('admin/order-history') || request()->is('admin/order-history/*') || request()->is('admin/order') || request()->is('admin/order/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.get.order.history') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-time"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Order History</span>
											</span></span></a></li>


            <li class="m-menu__item {{ (request()->is('admin/users') || request()->is('admin/users/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.users.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-user-add"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Users</span>
											</span></span></a></li>


            <!-- <li class="m-menu__item  {{ (request()->is('admin/cities') || request()->is('admin/cities/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.cities.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-location"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Cities</span>
											</span></span></a></li> -->
<!--
            <li class="m-menu__item  {{ (request()->is('admin/locations') || request()->is('admin/locations/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.locations.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-location"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Locations</span>
											</span></span></a></li> -->





            <li class="m-menu__item  {{ (request()->is('admin/products') || request()->is('admin/products/*'))? 'activeSubMenuItem' : ''}}"
                aria-haspopup="true" data-redirect="true"><a href="{{ route('admin.products.index') }}"
                                                             class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-line-graph"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Products</span>
											</span></span></a></li>

            <!-- <li class="m-menu__item  {{ (request()->is('admin/notification/index') )? 'activeSubMenuItem' : ''}}"
                aria-haspopup="true" data-redirect="true"><a href="{{ route('admin.notification.index') }}"
                                                             class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-line-graph"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Send Notification</span>
											</span></span></a></li> -->



        </ul>

    </div>

    <!-- END: Aside Menu -->
</div>
