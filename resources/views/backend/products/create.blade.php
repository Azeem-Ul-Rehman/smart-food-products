@extends('layouts.master')
@section('title','Products')
@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">{{ __('Products') }}</h3>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Add {{ __('Products') }}
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.products.store') }}"
                              enctype="multipart/form-data" role="form">
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">


                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="name"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Name') }} <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="name" type="text"
                                                   class="form-control @error('name') is-invalid @enderror"
                                                   name="name" value="{{ old('name') }}"
                                                   autocomplete="name" autofocus>

                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="categories"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Categories') }}
                                               </label>
                                            <input id="categories" type="text"
                                                   class="form-control @error('categories') is-invalid @enderror"
                                                   name="categories" value="{{ old('categories') }}"
                                                   autocomplete="categories" autofocus>

                                            @error('categories')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label for="quantity"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Quantity Per Box') }}
                                                <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="quantity" type="number"
                                                   class="form-control @error('quantity') is-invalid @enderror"
                                                   name="quantity" value="{{ old('quantity') }}"
                                                   autocomplete="quantity" autofocus>

                                            @error('quantity')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="amount"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Price Per Box') }}
                                                <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="amount" type="number"
                                                   class="form-control @error('amount') is-invalid @enderror"
                                                   name="amount" value="{{ old('amount') }}"
                                                   autocomplete="amount" autofocus>

                                            @error('amount')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="status"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Status') }}
                                                <span class="mandatorySign">*</span></label>
                                            <select id="status" name="status" class="form-control @error('amount') is-invalid @enderror"
                                                    autocomplete="status" autofocus>
                                                <option value="available" {{ (old('status') == 'available') ? 'selected' : ''  }}>Available</option>
                                                <option value="out_of_stock" {{ (old('status') == 'out_of_stock') ? 'selected' : ''  }}>Out of Stock</option>
                                            </select>

                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-12">
                                            <label for="description"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Description') }}
                                                <span class="mandatorySign">*</span></label>

                                            <textarea id="description"
                                                      class="form-control @error('description') is-invalid @enderror"
                                                      name="description" rows="5" cols="15"
                                                      autocomplete="description">{{ old('description') }}</textarea>

                                            @error('description')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>


                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-12">
                                            <label for="content"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Content') }}
                                               </label>

                                            <textarea id="content"
                                                      class="form-control @error('content') is-invalid @enderror"
                                                      name="content" rows="5" cols="15"
                                                      autocomplete="content">{{ old('content') }}</textarea>

                                            @error('content')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>


                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-6">
                                            <label for="image"
                                                   class="col-md-8 col-form-label text-md-left">{{ __('Banner Image') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input value="{{old('image')}}" type="file"
                                                   class="form-control @error('image') is-invalid @enderror"
                                                   onchange="readURL(this)" id="image"
                                                   name="image" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-thumbnail" style="display:none;"
                                                 id="img" src="#"
                                                 alt="your image"/>

                                            @error('image')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="heat_of_product"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Heat Rating') }}
                                             </label>
                                            <input id="heat_of_product" type="text"
                                                   class="form-control @error('heat_of_product') is-invalid @enderror"
                                                   name="heat_of_product" value="{{ old('heat_of_product') }}"
                                                   autocomplete="heat_of_product" autofocus>

                                            @error('heat_of_product')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.products.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection


