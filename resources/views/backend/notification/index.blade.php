@extends('layouts.master')
@section('title','Notification')
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">


            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.notification.send') }}"
                              enctype="multipart/form-data" role="form">
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">

                                    <div class="form-group row">


                                        <div class="col-md-12">
                                            <label for="message"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Message') }}
                                                <span class="mandatorySign">*</span></label>
                                            <textarea id="message" type="text"
                                                      class="form-control @error('message') is-invalid @enderror" required
                                                      autocomplete="message" autofocus>{{ old('message') }}</textarea>

                                            @error('message')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SEND') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush
