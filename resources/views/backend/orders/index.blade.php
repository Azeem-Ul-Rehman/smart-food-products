@extends('layouts.master')
@section('title','Orders')
@push('css')
    <style>
        .map-image {
            height: 60px;
            width: 60px;
            border-radius: 50px !important;
            margin-right: 15px;
            float: left;
        }

        .mw-100 {
            margin: 0 auto;
        }

    </style>
@endpush
@section('content')
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Orders History
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <span>
                                <span><strong>No of orders: {{ $orders->count() }}</strong> </span>
                            </span>
                        </li>
                    </ul>
                </div>

            </div>

            <div class="m-portlet__body">


                <form action="{{ route('admin.get.order.history') }}" method="GET" enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <h4>Filter</h4>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xl-3">


                            <label for="start_date" class="col-md-6 col-form-label text-md-left"><strong>Start
                                    Date:</strong></label>

                            <input type="date" class="form-control" id="start_date" name="start_date"
                                   value="{{ old('start_date') }}">


                        </div>
                        <div class="col-md-3 col-sm-3 col-xl-3">
                            <label for="end_date" class="col-md-6 col-form-label text-md-left"><strong>End
                                    Date:</strong></label>
                            <input type="date" class="form-control" id="end_date" name="end_date"
                                   value="{{ old('end_date') }}">
                        </div>
                        <div class="col-md-3 col-sm-3 col-xl-3">

                            <label for="status"
                                   class="col-md-4 col-form-label text-md-left"><strong>Status:</strong></label>

                            <select id="status"
                                    class="form-control @error('status') is-invalid @enderror"
                                    name="status" autocomplete="status">

                                <option value="">Select an option</option>
                                <option value="pending" {{ (request()->get('status') == 'pending') ? 'selected' : '' }}>
                                    Pending
                                </option>
                                <option
                                    value="confirmed" {{ (request()->get('status') == 'confirmed') ? 'selected' : '' }}>
                                    Confirmed
                                </option>
                                <option
                                    value="completed" {{ (request()->get('status') == 'completed') ? 'selected' : '' }}>
                                    Completed
                                </option>
                            </select>

                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <div class="m-form__actions m-form__actions">
                                <label style="display: block"
                                       class="col-md-4 col-form-label text-md-left">&nbsp;</label>
                                <a href="{{ route('admin.get.order.history') }}"
                                   class="btn btn-accent m-btn m-btn--icon m-btn--air refreshBtn">
                                <span>
                                    <i class="la la-refresh"></i>

                                </span>
                                </a>
                                <button class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </div>


                </form>


                <hr>
                {{--tableScroll--}}

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>

                        <th> Order No.</th>
                        <th> Customer Name</th>
                        <th> Shop Name</th>
                        <th width="10%"> Total Price</th>
                        <th width="10%"> Address</th>
                        <th>Order Status</th>
                        <th>Booking Date</th>
                        <th width="15%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($orders))
                        @foreach($orders as $key=>$order)
                            @if(!is_null($order->user))
                                <tr>
                                    <td>{{$order->order_id}}</td>
                                    <td>{{ucfirst($order->first_name)}} {{ ucfirst($order->last_name) }}</td>
                                    <td>{{ucfirst($order->area)}}</td>
                                    <td>£{{$order->total ?? 0}}</td>
                                    <td>{{$order->address ?? '--'}}</td>
                                    <td>
                                        <input type="hidden" value="{{$order->id}}" id="order_id">
                                        <select id="order_status" name="order_status"
                                                class="form-control @error('order_status') is-invalid @enderror"
                                                autocomplete="status" autofocus>
                                            <option
                                                value="pending" {{ (old('order_status',$order->order_status) == 'pending') ? 'selected' : ''  }}>
                                                Pending
                                            </option>
                                            <option
                                                value="confirmed" {{ (old('order_status',$order->order_status) == 'confirmed') ? 'selected' : ''  }}>
                                                Confirmed
                                            </option>
                                            <option
                                                value="completed" {{ (old('order_status',$order->order_status) == 'completed') ? 'selected' : ''  }}>
                                                Completed
                                            </option>
                                        </select>

                                    </td>
                                    <td>{{ date('d-m-Y', strtotime($order->created_at)) }}</td>
                                    {{--                                    <td>{{\Carbon\Carbon::parse($order->created_at)->setTimezone($order->time_zone)->format('d/m/Y h:i A')}}</td>--}}
                                    <td nowrap>
                                        <a href="{{route('admin.get.user.order.show',$order->id)}}"
                                           class="btn btn-sm btn-info pull-left ">View</a>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection
@push('models')

@endpush
@push('js')

    <script>


        var table = $("#m_table_1").DataTable({
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [5]}
            ],
        });

        $(document).on('change', '#order_status', function () {
            var order_id = $('#order_id').val();
            var order_status = $(this).val();

            var request = {"order_id": order_id, "order_status": order_status};
            if (order_status !== '' && order_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.update.order') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            toastr['success']("Order Status Updated Successfully");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                toastr['error']("Please Select Order Status");
            }


        });


    </script>



@endpush
