@extends('layouts.master')
@section('title','Orders')
@push('css')
    <style>
        .serviceInner {
            height: 280px;
        }

        .bannerFields {
            margin-top: 15px;
            margin-bottom: 30px;
        }

        .orderReviewDetails p {
            margin: 0;
        }

        .selectedServices {
            border: #2F7C02 solid 2px;
            padding: 15px 15px;
        }

        .btn:not(.btn-sm):not(.btn-lg) {
            line-height: 1.44;
            background-color: #2F7C02;
        }

        .serviceBoxHeader {
            color: #fff;
        }


        @media screen and (max-width: 450px)
        {
            .orderReviewDetails h4
            {
                margin: 5px 0;
            }
        }


    </style>
@endpush
@section('content')
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="">
                <div class="">
                    <div class="m-portlet">
                        <div class="container">
                            <div class="m-portlet__body">
                                <div class="container">
                                    <div class="serviceInnerMain">

                                        <div class="serviceBoxMain">
                                            <div class="serverInnerDetails">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 bannerFields">
                                                        <div class="row">
                                                            <div
                                                                class="col-md-12 col-sm-12 col-xs-12 orderReviewImage">

{{--                                                                @if($order->order_status == 'completed')--}}
{{--                                                                @else--}}
{{--                                                                    <p>Thank you for placing an order with Smart Food Products!--}}
{{--                                                                        Someone will be in touch with you shortly to--}}
{{--                                                                        confirm your products order.</p>--}}
{{--                                                                @endif--}}
                                                            </div>
                                                            <div
                                                                class="col-md-12 col-sm-12 col-xs-12 orderReviewDetails">
                                                                <div class="row">
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <p><b>Order ID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;         </b>{{ $order->order_id }}</p>
                                                                        <p><b>Customer Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;      </b>{{ ucfirst($order->first_name) }} {{ ucfirst($order->last_name) }}</p>
                                                                        <p><b>Phone Number: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;        </b>{{ $order->phone_number }}</p>
                                                                        <p><b>Email Address:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;      </b>{{ $order->email_address }}</p>
                                                                        <p><b>Shop Name: &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;       </b>{{ ucfirst($order->area) }}</p>
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <h4><b>Address</b></h4>
                                                                        <p>{{ucfirst($order->address) ?? '--'}}</p>
                                                                    </div>


                                                                </div>
                                                                <br>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="col-md-12 col-sm-12 col-xs-12 selectedServicesRight selectedServicesRight2 ">
                                                        <h4 class="selectedServicesRightHeading"><b>Products Ordered</b>
                                                        </h4>
                                                        <div class="selectedServices">
                                                            @if(!empty($order->order_details))
                                                                @foreach($order->order_details as $order_details)
                                                                    <div class="row">
                                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="row">
                                                                                <div
                                                                                    class="col-md-6 col-sm-6 col-xs-12">
                                                                                    <h4>{{ ucfirst($order_details->name) }}
                                                                                        x {{ $order_details->quantity }}</h4>
                                                                                </div>
                                                                                <div
                                                                                    class="col-md-6 col-sm-6 -col-xs-12 text-right">
                                                                                    <h4>
                                                                                        Rs. £{{ $order_details->amount * $order_details->quantity }}</h4>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>

                                                                        </div>

                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <hr style="margin: 10px 0; border: #2F7C02 dotted 1px; width: 100%;">
                                                                    </div>
                                                                @endforeach

                                                            @endif
                                                        </div>
                                                        <div class="serviceBoxHeader">
                                                            <div class="row">
                                                                <div class="col-md-12 col-sm-12 col-xs-12 text-left">
                                                                    <h5><strong>Grand Total</strong>
                                                                        <span style="float: right">
                                                                            <strong>Rs. <span
                                                                                    id="delivery-charges-price">£{{ $order->total }}</span></strong>
                                                                        </span>
                                                                    </h5>

                                                                </div>

                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('models')

@endpush
@push('js')
    <script>

    </script>
@endpush
