@extends('layouts.master')
@section('title','Locations')
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Edit {{ __('Location') }}
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.locations.update', $location->id) }}"
                              enctype="multipart/form-data" role="form">
                            @method('patch')
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">


                                    <div class="form-group row">


                                        <div class="col-md-12">
                                            <label for="address"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Shop Address') }}
                                                <span class="mandatorySign">*</span></label>
                                            <textarea id="address" type="text"
                                                   class="form-control @error('address') is-invalid @enderror"
                                                      autocomplete="address" autofocus>{{ old('address',$location->address) }}</textarea>

                                            @error('address')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-6">
                                            <label for="latitude"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Latitude') }} <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="latitude" type="text"
                                                   class="form-control @error('latitude') is-invalid @enderror"
                                                   name="latitude" value="{{ old('latitude',$location->latitude) }}"
                                                   autocomplete="latitude" autofocus>

                                            @error('latitude')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="longitude"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Longitude') }} <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="longitude" type="text"
                                                   class="form-control @error('longitude') is-invalid @enderror"
                                                   name="longitude" value="{{ old('longitude',$location->longitude) }}"
                                                   autocomplete="longitude" autofocus>

                                            @error('longitude')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.locations.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection


@push('js')
@endpush
