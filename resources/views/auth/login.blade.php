@extends('frontend.layout.app')
@section('title','Login')
@section('content')

    <div class="banner">
        <div class="container">
            <div class="col-md-6 col-sm-6"></div>
            <div class="col-md-6 col-sm-6 col-xs-12 bannerFields">
                <form method="POST" action="{{ route('login') }}" autocomplete="off">
                    @csrf
                    <div class="form-group">
                        <label for="email">Email <span>*</span></label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                               name="email" id="email" value="{{ old('email') }}"
                               autocomplete="email" placeholder="example@xyz.com"
                               autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password">Password <span>*</span></label>
                        <input id="password" type="password"
                               class="form-control @error('password') is-invalid @enderror" name="password"
                               required autocomplete="current-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">

                        <input class="form-check-input" type="checkbox" name="remember"
                               id="remember" {{ old('remember') ? 'checked' : '' }} value="1">
                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn buttonMain hvr-bounce-to-right">Sign In</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>

    </script>
@endpush
