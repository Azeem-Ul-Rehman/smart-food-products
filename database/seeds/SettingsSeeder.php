<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'name' => 'Helpline Support',
            'value' => 'xxxxxxxxxx',
        ]);

        Setting::create([
            'name' => 'Email Support',
            'value' => 'info@smartfoodProducts.com',
        ]);

        Setting::create([
            'name' => 'Find Us At',
            'value' => 'info@smartfoodProducts.com',
        ]);

        Setting::create([
            'name' => 'Whatsapp',
            'value' => 'https://web.whatsapp.com',
        ]);

        Setting::create([
            'name' => 'Instagram',
            'value' => 'http://instagram.com',
        ]);

        Setting::create([
            'name' => 'Facebook',
            'value' => 'http://facebook.com',
        ]);


    }
}
