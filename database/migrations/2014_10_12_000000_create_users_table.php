<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name')->nullable(); //required
            $table->string('last_name')->nullable();  //required
            $table->string('email')->unique()->nullable();  //required
            $table->string('password')->nullable();  //required
            $table->integer('city_id')->nullable();  //required
            $table->string('area_name')->nullable(); //required
            $table->integer('role_id')->nullable(); //required
            $table->string('gender')->nullable();  //required
            $table->string('phone_number')->nullable();  //required
            $table->text('address')->nullable();  //required
            $table->enum('user_type', ['admin', 'customer'])->nullable();
            $table->enum('status', ['pending', 'suspended', 'verified'])->default('pending');
            $table->string('profile_pic')->default('default.png');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
