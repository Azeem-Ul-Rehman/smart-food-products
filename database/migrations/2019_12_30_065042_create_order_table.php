<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('customer_id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email_address')->nullable();
            $table->string('company_name')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('country')->nullable();
            $table->longText('address')->nullable();
            $table->string('city')->nullable();
            $table->string('area')->nullable();
            $table->string('state')->nullable();
            $table->string('post_code')->nullable();
            $table->longText('special_instruction')->nullable();
            $table->string('time_zone')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('payment_status')->nullable();
            $table->longText('payment_transaction_id')->nullable();
            $table->string('order_id')->nullable();
            $table->double('sub_total')->nullable();
            $table->double('shipping_price')->nullable();
            $table->double('total')->nullable();
            $table->enum('order_status', ['pending', 'confirmed', 'completed']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
