<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//    Auth Controller
Route::post('login', 'Api\AuthController@login');
Route::post('register', 'Api\AuthController@register');
Route::post('forgot-password-request', 'Api\AuthController@forgotPasswordRequest');


//    General Controller
Route::get('cities', 'Api\GeneralController@cities');
Route::get('locations', 'Api\GeneralController@locations');
Route::get('settings', 'Api\GeneralController@settings');
Route::get('products', 'Api\GeneralController@products');


Route::group(['middleware' => 'auth:api'], function () {
    //    Auth Controller
    Route::post('logout', 'Api\AuthController@logout');
    Route::get('user', 'Api\AuthController@getUser');
    Route::post('fcm', 'Api\AuthController@updateFcmToken');


    //    General Controller

    Route::post('update-profile-image', 'Api\GeneralController@updateProfileImage');
    Route::post('update-profile', 'Api\GeneralController@updateProfile');
    Route::post('update-password', 'Api\GeneralController@updatePassword');

    //    Order Controller
    Route::get('orders', 'Api\OrderController@orders');
    Route::post('orders/store', 'Api\OrderController@store');


});

Route::fallback(function () {
    return response()->json(['message' => 'URL Not Found'], 404);
});
