// const firebaseConfig = {
    // apiKey: "AIzaSyBe4_7AROQIpN7IvHmMNNAP4_hQEPywJkc",
    // authDomain: "smartfoodproducts-86cd4.firebaseapp.com",
    // databaseURL: "https://smartfoodproducts-86cd4.firebaseio.com",
    // projectId: "smartfoodproducts-86cd4",
    // storageBucket: "smartfoodproducts-86cd4.appspot.com",
    // messagingSenderId: "882323583787",
    // appId: "1:882323583787:web:822b475e25df790c7b8b6e",
    // measurementId: "G-MWPZL5FJRY"


// };
// var firebaseConfig = {
//     apiKey: "AIzaSyB3rreznO2KWhT1MkaUyHwF35xvaTwO6_M",
//     authDomain: "smartfood-d0007.firebaseapp.com",
//     databaseURL: "https://smartfood-d0007.firebaseio.com",
//     projectId: "smartfood-d0007",
//     storageBucket: "smartfood-d0007.appspot.com",
//     messagingSenderId: "393604047299",
//     appId: "1:393604047299:web:930834a5bc77b9103c1d76",
//     measurementId: "G-VKV2T49HHS"
// };

const firebaseConfig = {
    apiKey: "AIzaSyB3rreznO2KWhT1MkaUyHwF35xvaTwO6_M",
    authDomain: "smartfood-d0007.firebaseapp.com",
    databaseURL: "https://smartfood-d0007.firebaseio.com",
    projectId: "smartfood-d0007",
    storageBucket: "smartfood-d0007.appspot.com",
    messagingSenderId: "393604047299",
    appId: "1:393604047299:web:930834a5bc77b9103c1d76",
    measurementId: "G-VKV2T49HHS"
};
firebase.initializeApp(firebaseConfig);


const messaging = firebase.messaging();
messaging.requestPermission().then(function () {
    console.log('Notification Permission Granted');
    return messaging.getToken();
}).then(function (token) {
    $('#device_token').val(token);
    console.log(token);
}).catch(function (err) {
    console.log("Unable to get permission to notify.", err);
});

messaging.onMessage((payload) => {
    console.log(payload);
});

