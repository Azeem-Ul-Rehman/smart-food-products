<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
        'city_id', 'area_name', 'role_id', 'gender',
        'phone_number', 'address',
        'user_type', 'status', 'profile_pic',
        'latitude', 'longitude',
        'fcm_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role')->withTimestamps();
    }

    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        abort(401, 'This action is unauthorized.');
    }

    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }

    public function fullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }


    public function orders()
    {
        return $this->hasMany(Order::class, 'customer_id', 'id');
    }

    public function completedOrders()
    {
        return $this->hasMany(Order::class, 'customer_id', 'id')->where('order_status', 'completed');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }


//     $this->middleware('role:SUPER_ADMIN');
//      $this->middleware('role:STAFF');
//      $this->middleware('role:CUSTOMER');
//      $this->middleware('role:DRIVER');

}
