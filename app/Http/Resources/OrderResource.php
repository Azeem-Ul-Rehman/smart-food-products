<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"                    => $this->id,
            "total_price"           => $this->total_price,
            "order_status"          => $this->order_status,
            "address"               => $this->address,
            "alternate_address"     => $this->alternate_address ?? "",
            "phone_number"          => $this->phone_number,
            "city"                  => $this->city->name,
            "area"                  => $this->area_name,
            "full_address"          => $this->address . ", " . $this->area_name . ", " . $this->city->name,

        ];
    }
}
