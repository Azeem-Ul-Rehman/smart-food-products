<?php

namespace App\Http\Resources\Customer;

use App\Http\Resources\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"                => $this->id,
            "name"              => $this->name,
            "product_id"        => $this->products_id,
            "amount"            => $this->amount,
            "quantity"          => $this->quantity,
            "product_detail"    => new ProductResource($this->product)

        ];
    }
}
