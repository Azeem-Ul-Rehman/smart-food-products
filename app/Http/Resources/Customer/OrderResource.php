<?php

namespace App\Http\Resources\Customer;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {


        return [
            "id"                     => $this->id,
            'order_id'               => $this->order_id,
            'first_name'             => $this->first_name,
            'last_name'              => $this->last_name,
            'email_address'          => $this->email_address,
            'phone_number'           => $this->phone_number,
            'address'                => $this->address,
            'shop_name'              => $this->area,
            'total'                  => $this->total,
            'order_status'           => $this->order_status,
            'delivery_charges'       => $this->shipping_price,
            'booking_date'           => $this->created_at,
            "order_detail"           => OrderDetailResource::collection($this->order_details),
            "customer"               => new UserResource($this->user),


        ];
    }
}
