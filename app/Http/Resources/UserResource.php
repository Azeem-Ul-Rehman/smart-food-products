<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        if ((!is_null($this->profile_pic))) {
            $profile_pic = $this->profile_pic;
        } else {
            $profile_pic = 'default.png';
        }
        return [
            "id"            => $this->id,
            "name"          => $this->first_name . " " . $this->last_name,
            "first_name"    => $this->first_name,
            "last_name"     => $this->last_name,
            "email"         => $this->email,
//            "city_id"       => $this->city_id,
//            "city_name"     => $this->city->name,
            "shop_name"     => $this->area_name,
//            "gender"        => $this->gender,
            "phone_number"  => $this->phone_number,
            "address"       => $this->address,
//            "user_type"     => $this->user_type,
            "status"        => $this->status,
            "fcm_token"     => $this->fcm_token,

//            "latitude"      => $this->latitude,
//            "longitude"     => $this->longitude,
//            "profile_pic"   => asset("/uploads/user_profiles/" . $profile_pic),


        ];
    }
}
