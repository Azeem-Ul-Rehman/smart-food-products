<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProductResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id"                => $this->id,
            "name"              => $this->name,
            "categories"        => $this->categories,
            "description"       => $this->description,
            "content"           => $this->content,
            "items_per_box"     => $this->quantity,
            "price_per_box"     => $this->amount,
            "status"            => $this->status,
            "image"             => asset('/uploads/products/' . $this->image),
            "heat_of_product"   => $this->heat_of_product,
            "created_at"        => $this->created_at,
            "updated_at"        => $this->updated_at,

        ];
    }
}
