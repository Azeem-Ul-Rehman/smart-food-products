<?php

namespace App\Http\Controllers\Backend;

use App\Events\RegisterEmail;
use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Category;
use App\Models\City;
use App\Models\Driver;
use App\Models\Membership;
use App\Models\Role;
use App\Models\Product;
use App\Models\Staff;
use App\Models\StaffServices;
use App\Models\User;
use App\Models\UserRole;
use App\Standards;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Events\SendReferralCodeWithPhone;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $users = User::orderBy('id', 'DESC')->get();


        return view('backend.users.index', compact('users'));
    }

    public function create()
    {
        $cities = City::all();
        $roles = Role::all();

        return view('backend.users.create', compact('cities', 'roles'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
//            'image' => '|image|mimes:jpg,jpeg,png|max:2048',
//            'city_id' => 'required|integer',
            'area_name' => 'required',
            'role_id' => 'required|integer',
//            'gender' => 'required',
            'phone_number' => 'required|unique:users,phone_number',
            'address' => 'required',
            'status' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',

        ], [
            'first_name.required' => 'First name  is required.',
            'last_name.required' => 'Last name  is required.',
//            'city_id.required' => 'City is required.',
            'role_id.required' => 'Role is required.',
            'area_name.required' => 'Shop Name  is required.',
//            'gender.required' => 'Gender  is required.',
            'phone_number.required' => 'Phone Number  is required.',
            'status.required' => 'Status  is required.',
            'email.required' => 'Email  is required.',
        ]);


        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {

            $profile_image = 'default.png';
        }

        $role = Role::find((int)$request->get('role_id'));


        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
//            'city_id' => $request->get('city_id'),
            'area_name' => $request->get('area_name'),
            'role_id' => $request->get('role_id'),
//            'gender' => $request->get('gender'),
            'phone_number' => $request->get('phone_number'),
            'address' => $request->get('address'),
            'user_type' => $role->name,
            'status' => $request->get('status'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
//            'profile_pic' => $profile_image,
            'fcm_token'      => $request->fcm_token
        ]);

        UserRole::create([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);


        return redirect()->route('admin.users.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'User created successfully.'
            ]);
    }

    public function edit($id)
    {
        $user = User::find($id);
        $cities = City::all();
        $roles = Role::all();
        return view('backend.users.edit', compact('user', 'cities', 'roles'));


    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
//            'image' => '|image|mimes:jpg,jpeg,png|max:2048',
//            'city_id' => 'required|integer',
            'area_name' => 'required',
            'role_id' => 'required|integer',
//            'gender' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
            'status' => 'required',
            'email' => 'required|string|email|max:255',
        ], [
            'first_name.required' => 'First name  is required.',
            'last_name.required' => 'Last name  is required.',
//            'city_id.required' => 'City is required.',
            'role_id.required' => 'Role is required.',
            'area_name.required' => 'Shop Name  is required.',
//            'gender.required' => 'Gender  is required.',
            'phone_number.required' => 'Phone Number  is required.',
            'status.required' => 'Status  is required.',
            'email.required' => 'Email  is required.',
        ]);

        $user = User::find($id);
        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = $user->profile_pic;
        }


        $role = Role::find((int)$request->get('role_id'));


        $user->update([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
//            'city_id' => $request->get('city_id'),
            'area_name' => $request->get('area_name'),
            'role_id' => $request->get('role_id'),
//            'gender' => $request->get('gender'),
            'phone_number' => $request->get('phone_number'),
            'address' => $request->get('address'),
            'user_type' => $role->name,
            'status' => $request->get('status'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
//            'profile_pic' => $profile_image,
            'fcm_token'      => $request->fcm_token
        ]);
        (new \App\Models\UserRole)->update([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);

        return redirect()->route('admin.users.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'User updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('admin.users.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'User has been deleted'
            ]);
    }
}
