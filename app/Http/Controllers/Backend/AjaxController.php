<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AjaxController extends Controller
{

    public function orderStatus(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'order_id' => 'required|exists:orders,id',
            'order_status' => 'required'
        ], [
            'order_id.required' => "Order is Required.",
            'order_id.exists' => "Invalid Order Selected."
        ]);

        if (!$validator->fails()) {
            $order = Order::where('id', $request->get('order_id'))->update([
                'order_status' => $request->get('order_status'),
            ]);


            $getOrder = Order::where('id' ,$request->order_id)->first();
            $user = User::where('id', $getOrder->customer_id)->first();
            if (!is_null($user->fcm_token) && $request->get('order_status') == 'confirmed') {
                $notification = new Notification();
                $notification->toSingleDevice($user->fcm_token, 'Smart Food Products', 'Your order has been ' . ucfirst($request->get('order_status')) . '. It will be delivered in 7 working days.', null, null);

            }


            $response['status'] = 'success';
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;

    }

}
