<?php

namespace App\Http\Controllers\Backend;

use App\Events\SendReferralCodeWithPhone;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Deal;
use App\Models\DealService;
use App\Models\DealSubCategory;
use App\Models\DealSubCategoryAddon;
use App\Models\Driver;
use App\Models\FirstOrderDiscount;
use App\Models\MembershipDiscount;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderMenuItemAddon;
use App\Models\ReferralDiscount;
use App\Models\Product;
use App\Models\Staff;
use App\Models\StaffServices;
use App\Models\User;
use App\Models\UserReferral;
use App\Traits\GeneralHelperTrait;
use Carbon\Carbon;
use Facade\Ignition\Support\Packagist\Package;
use Illuminate\Http\Request;
use function GuzzleHttp\Promise\all;

class OrderController extends Controller
{

    use GeneralHelperTrait;

    public function index(Request $request)
    {

        $orders = Order::whereNotNull('id');

        if (!is_null($request->status)) {

            $orders->where('order_status', $request->status);

        }

        if (!is_null($request->start_date)) {

            $orders->whereDate('created_at', '>=', $request->start_date);

        }

        if (!is_null($request->end_date)) {

            $orders->whereDate('created_at', '<=', $request->end_date);
        }

        $orders = $orders->get();
        return view('backend.orders.index', compact('orders'));
    }

    public function showOrder($id)
    {
        $order = Order::findOrFail($id);
        return view('backend.orders.show', compact('order'));

    }


    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        $order->delete();

        return redirect()->route('admin.get.order.history')->with([
            'flash_status' => 'success',
            'flash_message' => 'Order Deleted Successfully.',
        ]);
    }
}
