<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $users_count = User::all()->count();
        $total_orders = Order::all()->count();
        return view('backend.dashboard.index', compact('users_count', 'total_orders'));
    }
}
