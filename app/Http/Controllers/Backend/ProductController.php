<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('id', 'DESC')->get();

        return view('backend.products.index', compact('products'));
    }

    public function create()
    {
        return view('backend.products.create');

    }

    public function show($id)
    {

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'categories' => 'required|string',
            'image' => '|image|mimes:jpg,jpeg,png|max:2048',
            'description' => 'required',
            'quantity' => 'required|integer',
            'amount' => 'required|integer',
            'status' => 'required',
        ], [
            'amount.required' => 'Price Per Box is required',
            'amount.integer' => 'Price Per Box is integer',
            'quantity.required' => 'Quantity Per Box is required',
            'quantity.integer' => 'Quantity Per Box is integer',
        ]);
        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/products');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $banner_image = $name;
        } else {

            $banner_image = 'default.png';
        }
        Product::create([
            'name' => $request->get('name'),
            'categories' => $request->get('categories'),
            'description' => $request->get('description'),
            'content' => $request->get('content'),
            'quantity' => $request->get('quantity'),
            'amount' => $request->get('amount'),
            'heat_of_product' => $request->get('heat_of_product'),
            'image' => $banner_image,
            'status' => $request->get('status'),
        ]);
        return redirect()->route('admin.products.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Product created successfully.'
            ]);

    }

    public function edit($id)
    {
        $product = Product::find($id);
        return view('backend.products.edit', compact('product'));
    }

    public function update(Request $request, $id)
    {


        $this->validate($request, [
            'name' => 'required|string',
            'categories' => 'required|string',
            'image' => '|image|mimes:jpg,jpeg,png|max:2048',
            'description' => 'required',
            'quantity' => 'required|integer',
            'amount' => 'required|integer',
            'status' => 'required',
        ], [
            'amount.required' => 'Price Per Box is required',
            'amount.integer' => 'Price Per Box is integer',
            'quantity.required' => 'Quantity Per Box is required',
            'quantity.integer' => 'Quantity Per Box is integer',
        ]);
        $product = Product::find($id);
        if (isset($request->image) && $request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/products');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $banner_image = $name;
        } else {
            $banner_image = $product->image;
        }

        if ($product->status != $request->get('status')) {
            $users = User::whereNotNull('fcm_token')->where('role_id',2)->get();
            foreach ($users as $user) {
                $notification = new Notification();
                $notification->toSingleDevice($user->fcm_token, 'Smart Food Products', 'This ' . $product->name .' is '. ucwords(str_replace('_', ' ',$request->get('status'))).' ', null, null);

            }
        }
        $product->update([
            'name' => $request->get('name'),
            'categories' => $request->get('categories'),
            'description' => $request->get('description'),
            'content' => $request->get('content'),
            'quantity' => $request->get('quantity'),
            'amount' => $request->get('amount'),
            'heat_of_product' => $request->get('heat_of_product'),
            'image' => $banner_image,
            'status' => $request->get('status'),
        ]);


        return redirect()->route('admin.products.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Product updated successfully.'
            ]);

    }

    public function destroy($id)
    {
        $service = Product::findOrFail($id);
        $service->delete();

        return redirect()->route('admin.products.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Product has been deleted'
            ]);
    }
}
