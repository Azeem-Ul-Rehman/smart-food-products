<?php

namespace App\Http\Controllers\Backend;

use App\Models\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:admin');
    }



    public function index()
    {
        $locations = Location::all();

        return view('backend.locations.index', compact('locations'));
    }


    public function create()
    {
        return view('backend.locations.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'address'       => 'required',
            'latitude'      => 'required',
            'longitude'     => 'required',
        ],[
            'address.required'      => 'Shop Address is required.',
            'latitude.required'     => 'Latitude is required.',
            'longitude.required'    => 'Longitude is required.'
        ]);
        Location::create($request->all());
        return redirect()->route('admin.locations.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Location created successfully.'
            ]);
    }


    public function show(Location $location)
    {
        //
    }


    public function edit($id)
    {
        $location = Location::find($id);
        return view('backend.locations.edit', compact('location'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'address'       => 'required',
            'latitude'      => 'required',
            'longitude'     => 'required',
        ],[
            'address.required'      => 'Shop Address is required.',
            'latitude.required'     => 'Latitude is required.',
            'longitude.required'    => 'Longitude is required.'
        ]);
        Location::find($id)->update($request->all());
        return redirect()->route('admin.locations.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Location updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $location = Location::findOrFail($id);
        $location->delete();

        return redirect()->route('admin.locations.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Location has been deleted'
            ]);
    }
}
