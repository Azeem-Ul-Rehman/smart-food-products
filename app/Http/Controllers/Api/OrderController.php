<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Resources\Customer\OrderResource;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\User;
use App\Traits\GeneralHelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    use GeneralHelperTrait;

    public function orders()
    {
        try {

            $user = Auth::user();
            return response()->json(['data' => OrderResource::collection(Order::where('customer_id', $user->id)->orderBy('id', 'desc')->get())], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function store(Request $request)
    {
        $finalOrder = json_decode($request->finalOrder, true);
        $validator = Validator::make($finalOrder, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email_address' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
            'total' => 'required',
            'shop_name' => 'required',
            'delivery_charges' => 'required',
        ], [
            'shop_name.required' => 'Shop Name is required',
            'delivery_charges.required' => 'Delivery Charges is required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {

            if (!empty($finalOrder)) {
                $user = Auth::user();
                $order_id = $this->orderNumber();

                $order = Order::create([

                    'customer_id' => $user->id,
                    'order_id' => $order_id,
                    'first_name' => $finalOrder['first_name'],
                    'last_name' => $finalOrder['last_name'],
                    'area' => $finalOrder['shop_name'],
                    'email_address' => $finalOrder['email_address'],
                    'phone_number' => $finalOrder['phone_number'],
                    'address' => $finalOrder['address'],
                    'shipping_price' => $finalOrder['delivery_charges'],
                    'total' => $finalOrder['total'],
                    'order_status' => 'pending',

//                    'company_name'           => $finalOrder['company_name'],
//                    'country'                => $finalOrder['country'],
//                    'city'                   => $finalOrder['city'],
//                    'state'                  => $finalOrder['state'],
//                    'post_code'              => $finalOrder['post_code'],
//                    'special_instruction'    => $finalOrder['special_instruction'],
//                    'sub_total'              => $finalOrder['sub_total'],
//                    'shipping_price'         => $finalOrder['shipping_price'],
//                    'payment_method'         => $finalOrder['payment_method'],
//                    'time_zone'              => $finalOrder['timeZone'],

                ]);


                if (!empty($finalOrder['products'])) {
                    foreach ($finalOrder['products'] as $products) {

                        $productData = Product::where('id', (int)$products['product_id'])->first();
                        if (!is_null($productData)) {
                            $order_details = OrderDetail::create([
                                'order_id' => $order->id,
                                'products_id' => $products['product_id'],
                                'name' => $productData->name,
                                'quantity' => $products['quantity'],
                                'amount' => $productData->amount,
                            ]);
                        }


                    }

                }



                $message = 'Thank you for your order with Smart Food Products!';
                $message2 = 'Someone from our team will be calling you shortly to confirm your order.Your Order ID is:' . $order_id . '.';
                return response()->json(['status' => true, 'message' => $message, 'message2' => $message2, 'data' => OrderResource::collection(Order::where('id', $order->id)->get())], 200);
            } else {
                return response()->json(['status' => true, 'message' => 'Data  not found'], 401);
            }

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }


    }

}
