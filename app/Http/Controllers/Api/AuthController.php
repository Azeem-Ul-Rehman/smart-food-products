<?php

namespace App\Http\Controllers\Api;


use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;
use App\Models\UserRole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{
    public $successStatus = 200;

    public function forgotPasswordRequest(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {

            $user = User::where('email', $request->email)->first();

            if (is_null($user) || is_null($user->email) || empty($user->email)) {
                return response()->json(["status" => 'error', "message" => 'Your account is not associated with this email address.'], 404);
            }

            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json(['status' => 'success', "message" => 'Your password has been changes successfully.'], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
//            'city_id' => 'required|integer',
            'shop_name' => 'required',
            'phone_number' => 'required|unique:users,phone_number',
//            'gender' => 'required',
            'address' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
            'fcm_token' =>'required'
        ], [
            'first_name.required' => 'First name is required.',
            'last_name.required' => 'Last name is required.',
//            'city_id.required' => 'City is required.',
            'shop_name.required' => 'Shop Name is required.',
//            'gender.required' => 'Gender is required.',
            'phone_number.required' => 'Phone Number is required.',
            'address.required' => 'Address is required.',
            'email.required' => 'Email is required.',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = 'default.png';
        }

        $role = Role::find(2);
        $user = User::create([
            'first_name'     => $request->get('first_name'),
            'last_name'      => $request->get('last_name'),
//            'city_id'        => $request->get('city_id'),
            'area_name'      => $request->get('shop_name'),
            'role_id'        => $role->id,
//            'gender'         => $request->get('gender'),
            'phone_number'   => $request->get('phone_number'),
            'address'        => $request->get('address'),
            'user_type'      => $role->name,
            'email'          => $request->get('email'),
            'password'       => bcrypt($request->get('password')),
            'profile_pic'    => $profile_image,
            'fcm_token'      => $request->fcm_token
        ]);

        UserRole::create([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);


        $userData = User::where('email',$request->email)->first();
        $token = $user->createToken('AppName')->accessToken;
        $message = 'Thank you for becoming a member of Smart Food Products!';
        return response()->json(['message' => $message, 'data' => new UserResource($userData) , 'token'=>$token], 201);
    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'password' => 'required'
        ],[
            'email.required' =>'Email is required',
            'password.required' =>'Password is required',

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {


            $user = Auth::user();
            if ($user->status == 'suspended') {

                return response()->json(['message' => 'Your account is suspended.'], 401);
            }

            $success['token'] = $user->createToken('AppName')->accessToken;
            $success['data'] = new UserResource($user);
            return response()->json(['success' => $success], $this->successStatus);
        } else {
            return response()->json(['message' => 'Invalid Password'], 401);
        }
    }
    public function updateFcmToken(Request  $request)
    {

        $validator = Validator::make($request->all(), [
            'fcm_token' => 'required'
        ],[
            'fcm_token.required' =>'FCM Token is required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }


            $user = Auth::user();
            $user->fcm_token = $request->fcm_token;
            $user->save();

            return response()->json(['message' => 'FCM Toekn updated successfully', 'data' => new UserResource($user)], 201);




    }
    public function logout()
    {
        try {
            $user = Auth::user();
            $user->fcm_token = null;
            $user->save();

            foreach ($user->tokens as $token) {
                $token->revoke();
                $token->delete();
            }

            return response()->json(['status' => true], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

        return response()->json(null, 204);
    }
    public function getUser()
    {
        $user = new UserResource(auth()->user());
        return response()->json(['success' => $user], $this->successStatus);
    }


}
