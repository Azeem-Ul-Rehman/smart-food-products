<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Http\Resources\OrderResource;
use App\Http\Resources\PackageResource;
use App\Http\Resources\UserResource;
use App\Models\Area;
use App\Models\Category;
use App\Models\City;
use App\Models\Coupon;
use App\Models\CouponUser;
use App\Models\Deal;
use App\Models\FirstOrderDiscount;
use App\Models\Location;
use App\Models\MembershipDiscount;
use App\Models\Order;
use App\Models\ReferralDiscount;
use App\Models\Product;
use App\Models\Setting;
use App\Models\User;
use App\Models\UserReferral;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class GeneralController extends Controller
{
    public function cities()
    {
        try {
            return response()->json(['data' => City::orderBy('name', 'asc')->get()], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function locations()
    {
        try {
            return response()->json(['data' => Location::all()], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function settings()
    {
        try {

            return response()->json(['data' => Setting::get()], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }
    public function products()
    {
        try {

            $user = Auth::user();
            return response()->json([
                'data'              => ProductResource::collection(Product::all()),
                'delivery_charges'  => '8.99'
            ],
                200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }
    public function updateProfileImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'mimes:jpeg,jpg,png|required|max:2048',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {

            $user = Auth::user();
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $image->move($destinationPath, $name);
            $profile_image = $name;


            $user->update([
                'profile_pic' => $profile_image,
            ]);
            return response()->json(['status' => true, 'message' => 'Profile image updated successfully.', 'data' => new UserResource($user)], 200);


        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }
    public function updateProfile(Request $request)
    {

        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'first_name'        => 'required|string',
            'last_name'         => 'required|string',
            'shop_name'         =>'required',
            'email'             => 'required|string|email|max:255|unique:users,email,'. $user->id,
            'phone_number'      => 'required|unique:users,phone_number,' . $user->id,
            'address'           => 'required',

        ],[
            'shop_name.required' => 'Shop Name is required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {

            $user->update([
                'first_name'    => $request->first_name,
                'last_name'     => $request->last_name,
                'shop_name'     => $request->shop_name,
                'email'         => $request->email,
                'phone_number'  => $request->phone_number,
                'address'       => $request->address,

            ]);
            return response()->json(['status' => true, 'message' => 'Profile updated successfully.','data' => new UserResource($user)], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }

    }
    public function updatePassword(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'current-password' => 'required',
            'new-password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        try {

            if (!(Hash::check($request->get('current-password'), $user->password))) {
                // The passwords matches
                return response()->json(['status' => 'error', 'message' => 'Your current password does not matches with the password you provided. Please try again.'], 200);
            }

            if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
                //Current password and new password are same
                return response()->json(['status' => 'error', 'message' => 'New Password cannot be same as your current password. Please choose a different password.'], 200);
            }

            if (strcmp($request->get('new-password-confirm'), $request->get('new-password')) == 0) {
                //Change Password
                $user->password = bcrypt($request->get('new-password'));
                $user->save();

                return response()->json(['status' => 'success', 'message' => 'Password Changed successfully.'], 200);

            } else {
                return response()->json(['status' => 'error', 'message' => 'New Password must be same as your confirm password.'], 200);
            }
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }


    }


}
